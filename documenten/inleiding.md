﻿Inleiding
=======================================

|Informatie||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Jonas Pottie|
|Opleiding|Bachelor in de grafische en digitale media|
|Academiejaar|2015-16|
|Opleidingsonderdeel|New Media Design & Development I (NMDAD I)|

***

[TOC]

***

|Info||
|--|--|
|Studiepunten|6|
|Contacturen|4|
|Docenten|Philippe De Pauw - Waterschoot, Jonas Pottie|
|Evaluatie 1ste examenkans|Werkstuk (maximaal in groepjes van 3). Mondelinge verdediging van werkstuk(ken) + vragen|
|Evaluatie 2de examenkans|Werkstuk (Individueel). Mondelinge verdediging van werkstuk + vragen|
|Begincompetenties|2D Animation & Webdesign II|
|ECTS|[ECTS Fiche NMDADI](http://www.arteveldehogeschool.be/ects/ahsownapp/ects/ECTSFiche.aspx?olodID=42086)|

**2MMP onbekend**

##Inhoud

##Studiemateriaal

- Chamilo cursus NMDAD-I
- Bitbucket Respository [NMDADI_201516](https://bitbucket.org/drdynscript/nmdadi_201516)
- Online links, tutorials, MOOC's and samples

##Tools

- <http://git-scm.com/>
- <http://www.sourcetreeapp.com/>
- <http://www.jetbrains.com/phpstorm/>
- <https://bitbucket.org/>
- <http://courses.olivierparent.be/servermanagement/local-development-environment/installing-design-and-development-tools/source-control/>

##Technologieën

- HTML
- CSS
- JavaScript
- CSS pre-processors
- Static Site Generators
- Commandline and Automation tools
- JS bibliotheken & frameworks, zoals: jQuery, Normalize, Modernizr, Respond.js, Underscore.js, Lodash.js, Crossroads.js, Hashes.js, ...

##Software

- **Adobe CC**
- SublimeText
- **PHP Storm 7+**
- ...

##Code PHPStorm

Zie chamilo bij de aankondigingen cursus NMDAD-I