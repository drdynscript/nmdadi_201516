Cursus New Media Design & Development I
=======================================

|Informatie||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Jonas Pottie|
|Opleiding|Bachelor in de grafische en digitale media|
|Academiejaar|2015-16|
|Opleidingsonderdeel|New Media Design & Development I (NMDAD I)|
|Aantal lesweken|12|
|aantal studiepunten|6|
|Aantal contacturen|48 = 72 uren|
|Te presteren uren incl. colleges|minimaal 180 uren|

***

[TOC]

***

Documenten
---------------

* Week 1
	* [Inleiding](./documenten/inleiding.md)
	* [Werkstuk](./documenten/werkstuk.md)
	* [Groepsindeling werkstuk](./documenten/werkstuk_groepsindeling.md)
	* [Markdown](./documenten/markdown.md)
	* [Taken week 1](./taken/taken_week1.md)



Downloads
-------------

- Je kan het wolkje in BitBucket aanklikken om al het cursusmateriaal te downloaden
- Je kan ook eerst het cursusmateriaal lokaal klonen: `git clone https://drdynscript@bitbucket.org/drdynscript/nmdadi_201516.git`
- Ga naar de folder `nmdadi_201516` via commandline `cd nmdadi_201516`
- Daarna ga je de wijzigingen lokaal binnentrekken: `git pull -u origin master`

Docenten
--------
**Philippe De Pauw - Waterschoot**

* <http://twitter.com/drdynscript>
* <http://bitbucket.org/drdynscript>
* <https://www.linkedin.com/pub/philippe-de-pauw/>
* <philippe.depauw@arteveldehs.be>

**Jonas Pottie**

* <http://twitter.com/jonaspottie>
* <https://be.linkedin.com/in/jonaspottie>
* <jonas.pottie@arteveldehs.be>

Arteveldehogeschool
-------------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>


Copyright and license
--------------------------

Cursusmateriaal copyright 2003-2015 Arteveldehogeschool | Opleiding Grafische en Digitale Media | drdynscript. Cursusmateriaal, zoals code, documenten, ... uitgebracht onder de Creative Commons licentie.