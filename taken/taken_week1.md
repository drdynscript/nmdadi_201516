﻿Taken Week 1 - New Media Design & Development I
===============================================

|Informatie||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Jonas Pottie|
|Opleiding|Bachelor in de grafische en digitale media|
|Academiejaar|2015-16|
|Opleidingsonderdeel|New Media Design & Development I (NMDAD I)|

***

[TOC]

***

Taak 1: GitHub en bitbucket
---------------------------
* Registreer je voor het [GitHub Student Developer Pack]("https://education.github.com/pack").
	* Gebruik als naam jouw loginnaam van de Arteveldehogeschool.
	* Gebruik jouw email-adres van de arteveldehogeschool.
	* In het veld School name tijp je hetvolgende in: Artevelde University College Ghent.
	* In het veld Graduation year selecteer je 2017, 2018 of 2019.
	* In het grote tekstveld (textarea) geef je aan waarvoor je dit aanbod wil gebruiken, bv.: `I will use this Student Developer Pack for usage in different courses`. Het aanvaarder van deze aanvraag kan enkele weken duren. Daarom zullen we ook rechtstreeks registereren in GitHub.
* Registreer je in [GitHub]("https://github.com/join")
	* Gebruik als username jouw loginnaam van de Arteveldehogeschool.
	* Gebruik jouw email-adres van de arteveldehogeschool.
* Registreer je vervolgens in [Bitbucket]("https://bitbucket.org/account/signin/?next=/") via jouw reeds aangemaakt GitHub account.
* Voeg een persoonlijk foto van jouw gezicht toe aan jouw account binnen Bitbucket.

Taak 2: Résumé Markdown Document
--------------------------------

* Maak een nieuwe repository in Bitbucket aan met de naam: `nmdadi_2015-2016`.
* Geef de docenten Philippe De Pauw - Waterschoot, Jonas Pottie en Olivier Parent leestoegang tot deze repository. 
* Geef aan deze repository een uniek logo.
* Maak in deze aangemaakte repository het bestand resume.md aan.
