/* TEAM */
Company: Bachelor in de grafische en digitale media - Arteveldehogeschool
Website: http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media
Twitter: @bachelorGDM
Email: info [dot] grafische [dot] digitalemedia [at] arteveldehs [dot] be
Location: Industrieweg 232, 9030 Gent-Mariakerke (Belgium)
Telephone: +32 9 234 86 00

Development: Philippe De Pauw - Waterschoot
Website: http://www.drdynscript.eu
Twitter: @drdynscript
Email: philippe [dot] depauw [at] arteveldehs [dot] be
Location: Ghent, Belgium

UX: Jonas Pottie
Twitter: @jonaspottie
Email: jonas [dot] pottie [at] arteveldehs [dot] be
Location: Ghent, Belgium


/* THANKS */
Many many thanx for this course :)


/* SITE */
Last update: Always up-to-date
Standards: HTML5, CSS3, JavaScript
Components: Modernizr, jQuery, Google Maps, Handlebars, Jade, Emmet, ...
Software: PHPStorm, Adobe CC