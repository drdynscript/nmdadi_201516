﻿Groepsindeling werkstuk
=======================================

|Informatie||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Jonas Pottie|
|Opleiding|Bachelor in de grafische en digitale media|
|Academiejaar|2015-16|
|Opleidingsonderdeel|New Media Design & Development I (NMDAD I)|

***

[TOC]

***

##Ingeschreven studenten

**2MMPa alias proDEV (35 studenten)**

**2MMPb alias proDUCE (44 studenten)**

**2MMPc alias proDUCE (44 studenten)**

**2MMP onbekend**

##Groepsindeling

Groepsindeling zal plaatsvinden binnen elke klas, dus **niet over klassen heen**.

**2MMPa**

|Groepsnummer|Studenten|
|2MMPA-01||
|2MMPA-02|
|2MMPA-03||
|2MMPA-04||
|2MMPA-05||
|2MMPA-06||
|2MMPA-07||
|2MMPA-08||
|2MMPA-09||
|2MMPA-10||
|2MMPA-11||
|2MMPA-12||
|2MMPA-13||
|2MMPA-14||
|2MMPA-15||
|2MMPA-16||
|2MMPA-17||
|2MMPA-18||
|2MMPA-19||

**2MMPb**

|Groepsnummer|Studenten|
|2MMPB-01||
|2MMPB-02|
|2MMPB-03||
|2MMPB-04||
|2MMPB-05||
|2MMPB-06||
|2MMPB-07||
|2MMPB-08||
|2MMPB-09||
|2MMPB-10||
|2MMPB-11||
|2MMPB-12||
|2MMPB-13||
|2MMPB-14||
|2MMPB-15||
|2MMPB-16||
|2MMPB-17||
|2MMPB-18||

**2MMPc**

|Groepsnummer|Studenten|
|2MMPC-01||
|2MMPC-02|
|2MMPC-03||
|2MMPC-04||
|2MMPC-05||
|2MMPC-06||
|2MMPC-07||
|2MMPC-08||
|2MMPC-09||
|2MMPC-10||
|2MMPC-11||
|2MMPC-12||
|2MMPC-13||
|2MMPC-14||
|2MMPC-15||
|2MMPC-16||
|2MMPC-17||
|2MMPC-18||
|2MMPC-19||
|2MMPC-19||


